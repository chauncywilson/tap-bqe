# tap-bqe

`tap-bqe` is a Singer tap for BQE.

Built with the [Meltano Tap SDK](https://sdk.meltano.com) for Singer Taps.



## Installation

Install from PyPi:

```bash
pipx install tap-bqe
```

Install from GitHub:

```bash
pipx install git+https://gitlab.com/chauncywilson/tap-bqe@main
```



### Accepted Config Options

BQE tap class.

Built with the [Meltano Singer SDK](https://sdk.meltano.com).

## Capabilities

* `catalog`
* `state`
* `discover`
* `about`
* `stream-maps`
* `schema-flattening`
* `batch`

## Settings

| Setting             | Required | Default | Description |
|:--------------------|:--------:|:-------:|:------------|
| grant_type          | True     | None    |             |
| redirect_uri        | True     | None    |             |
| code                | False    | None    |             |
| client_id           | True     | None    |             |
| client_secret       | True     | None    |             |
| state               | False    | None    |             |
| stream_maps         | False    | None    | Config object for stream maps capability. For more information check out [Stream Maps](https://sdk.meltano.com/en/latest/stream_maps.html). |
| stream_map_config   | False    | None    | User-defined config values to be used within map expressions. |
| flattening_enabled  | False    | None    | 'True' to enable schema flattening and automatically expand nested properties. |
| flattening_max_depth| False    | None    | The max depth to flatten schemas. |
| batch_config        | False    | None    |             |

A full list of supported settings and capabilities is available by running: `tap-bqe --about`

## Supported Python Versions

* 3.8
* 3.9
* 3.10
* 3.11
* 3.12
* 3.13
* 3.14
* 3.15
* 3.16

A full list of supported settings and capabilities for this
tap is available by running:

```bash
tap-bqe --about
```

### Configure using environment variables

This Singer tap will automatically import any environment variables within the working directory's
`.env` if the `--config=ENV` is provided, such that config values will be considered if a matching
environment variable is set either in the terminal context or in the `.env` file.

### Source Authentication and Authorization

tap-bqe requires an openId code and a bearer OAuth 2.0 token provided by BQE Core

## Usage

You can easily run `tap-bqe` by itself or in a pipeline using [Meltano](https://meltano.com/).

### Executing the Tap Directly

```bash
tap-bqe --version
tap-bqe --help
tap-bqe --config CONFIG --discover > ./catalog.json
```

## Developer Resources

Follow these instructions to contribute to this project.

### Initialize your Development Environment

```bash
pipx install poetry
poetry install
```

### Create and Run Tests

Create tests within the `tests` subfolder and
  then run:

```bash
poetry run pytest
```

You can also test the `tap-bqe` CLI interface directly using `poetry run`:

```bash
poetry run tap-bqe --help
```

### Testing with [Meltano](https://www.meltano.com)

_**Note:** This tap will work in any Singer environment and does not require Meltano.
Examples here are for convenience and to streamline end-to-end orchestration scenarios._

Next, install Meltano (if you haven't already) and any needed plugins:

```bash
# Install meltano
pipx install meltano
# Initialize meltano within this directory
cd tap-bqe
meltano install
```

Now you can test and orchestrate using Meltano:

```bash
# Test invocation:
meltano invoke tap-bqe --version
# OR run a test `elt` pipeline:
meltano elt tap-bqe target-jsonl
```

### SDK Dev Guide

See the [dev guide](https://sdk.meltano.com/en/latest/dev_guide.html) for more instructions on how to use the SDK to
develop your own taps and targets.
