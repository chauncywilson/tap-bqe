"""Stream type classes for tap-bqe."""

from __future__ import annotations

import typing as t
from pathlib import Path

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_bqe.client import BQEStream


class AccountStream(BQEStream):
    """Define custom stream."""

    name = "account"
    path = "account"
    primary_keys: t.ClassVar[list[str]] = ["id"]
    replication_key = "lastUpdated"
    schema = th.PropertiesList(
        th.Property("code", th.StringType),
        th.Property("name", th.StringType),
        th.Property("type", th.IntegerType, 
                    description="""0=Accounts Payable, 1=Accounts Receivable, 2=Bank account,
                      #3=Cost of Goods Sold account, 4=Credit card account, 5=Equity account,
                      #6=Expense account, 7=Fixed Asset account, 8=Income account,
                      #9=Long Term Liability account, 10=Non Posting account, 11=Other Asset account
                      #12=Other Current Asset account, 13=Other Current Liability account,
                      #14=Other Expense account, 15=Other Income account, 16=Not Defined account"""),
        th.Property("createdById", th.UUIDType),
        th.Property("createdOn", th.DateTimeType),
        th.Property("customFields", th.ArrayType(th.ObjectType(
            th.Property("definitionId", th.UUIDType),
            th.Property("CreatedById", th.UUIDType),
            th.Property("CreatedOn", th.DateTimeType),
            th.Property("description", th.StringType),
            th.Property("id", th.UUIDType),
            th.Property("label", th.StringType),
            th.Property("lastUpdated", th.DateTimeType),
            th.Property("lastUpdatedById", th.UUIDType),
            th.Property("objectState", th.IntegerType),
            th.Property("token", th.IntegerType),
            th.Property("type", th.IntegerType),
            th.Property("value", th.StringType),
            th.Property("version", th.StringType),
        ))),
        th.Property("description", th.StringType),
        th.Property("displayAccount", th.StringType),
        th.Property("id", th.UUIDType),
        th.Property("isActive", th.BooleanType),
        th.Property("lastUpdated", th.DateTimeType),
        th.Property("lastUpdatedById", th.StringType),
        th.Property("level", th.NumberType),
        th.Property("objectState", th.StringType),
        th.Property("openingBalance", th.NumberType),
        th.Property("openingBalanceAsOf", th.DateTimeType),
        th.Property("parentAccount", th.StringType),
        th.Property("parentAccountId", th.StringType),
        th.Property("rootAccountId", th.StringType),
        th.Property("routingNumber", th.StringType),
        th.Property("runningBalance", th.NumberType),
        th.Property("taxLine", th.StringType),
        th.Property("token", th.NumberType),
        th.Property("version", th.StringType),
    ).to_dict()

class BillStream(BQEStream):
    name = "bill"
    path = "bill"
    replication_key = "lastUpdated"
    schema = th.PropertiesList(
        th.Property("date", th.DateTimeType),
        th.Property("number", th.StringType),
        th.Property("vendorId", th.UUIDType),
        th.Property("amount", th.NumberType),
        th.Property("balance", th.NumberType),
        th.Property("customFields", th.ArrayType(th.ObjectType(
            th.Property("definitionId", th.UUIDType),
            th.Property("CreatedById", th.UUIDType),
            th.Property("CreatedOn", th.DateTimeType),
            th.Property("description", th.StringType),
            th.Property("id", th.UUIDType),
            th.Property("label", th.StringType),
            th.Property("lastUpdated", th.DateTimeType),
            th.Property("lastUpdatedById", th.UUIDType),
            th.Property("objectState", th.IntegerType),
            th.Property("token", th.IntegerType),
            th.Property("type", th.IntegerType),
            th.Property("value", th.StringType),
            th.Property("version", th.StringType),
        ))),
        th.Property("dueDate", th.DateTimeType),
        th.Property("id", th.UUIDType),
        th.Property("memo", th.StringType),
        th.Property("referenceNumber", th.StringType),
        th.Property("reimbursable", th.BooleanType),
        th.Property("term", th.StringType),
        th.Property("termId", th.UUIDType),
        th.Property("vendor", th.StringType),
        th.Property("accountSplits", th.ObjectType(
            th.Property("accrual", th.ArrayType(
                th.ObjectType(
                    th.Property("account", th.StringType),
                    th.Property("accountId", th.StringType),
                    th.Property("amount", th.NumberType),
                    th.Property("isDebit", th.BooleanType),
                    th.Property("isParent", th.BooleanType),
                    th.Property("transactionType", th.StringType),
                )
            )),
            th.Property("cash", th.ArrayType(
                th.ObjectType(
                    th.Property("account", th.StringType),
                    th.Property("accountId", th.StringType),
                    th.Property("amount", th.NumberType),
                    th.Property("isDebit", th.BooleanType),
                    th.Property("isParent", th.BooleanType),
                    th.Property("transactionType", th.StringType),
                )
            )))),
        th.Property("expenseItems", th.ArrayType(
            th.ObjectType(
                th.Property("accountId", th.StringType),
                th.Property("amount", th.NumberType),
                th.Property("account", th.StringType),
                th.Property("class", th.StringType),
                th.Property("classId", th.StringType),
                th.Property("createdById", th.StringType),
                th.Property("createdOn", th.DateTimeType),
                th.Property("entity", th.StringType),
                th.Property("entityId", th.StringType),
                th.Property("entityType", th.StringType),
                th.Property("id", th.StringType),
                th.Property("lastUpdated", th.DateTimeType),
                th.Property("lastUpdatedById", th.StringType),
                th.Property("memo", th.StringType),
                th.Property("objectState", th.StringType),
                th.Property("token", th.NumberType),
                th.Property("version", th.StringType),
            )
        )),
        th.Property("lineItems", th.ArrayType(
            th.ObjectType(
                th.Property("itemId", th.StringType),
                th.Property("itemType", th.StringType),
                th.Property("projectId", th.StringType),
                th.Property("rate", th.NumberType),
                th.Property("units", th.NumberType),
                th.Property("amount", th.NumberType),
                th.Property("billable", th.BooleanType),
                th.Property("billRate", th.NumberType),
                th.Property("class", th.StringType),
                th.Property("classId", th.StringType),
                th.Property("createdById", th.StringType),
                th.Property("createdOn", th.DateTimeType),
                th.Property("description", th.StringType),
                th.Property("expenseAccount", th.StringType),
                th.Property("expenseAccountId", th.StringType),
                th.Property("extra", th.BooleanType),
                th.Property("id", th.StringType),
                th.Property("incomeAccount", th.StringType),
                th.Property("incomeAccountId", th.StringType),
                th.Property("item", th.StringType),
                th.Property("lastUpdated", th.DateTimeType),
                th.Property("lastUpdatedById", th.StringType),
                th.Property("markup", th.NumberType),
                th.Property("memo", th.StringType),
                th.Property("objectState", th.StringType),
                th.Property("project", th.StringType),
                th.Property("purchaseTaxRate", th.NumberType),
                th.Property("token", th.NumberType),
                th.Property("version", th.StringType),
        ))),
        th.Property("workflow", th.ArrayType(
            th.ObjectType(
                th.Property("actionById", th.StringType),
                th.Property("action", th.StringType),
                th.Property("actionBy", th.StringType),
                th.Property("createdById", th.StringType),
                th.Property("createdOn", th.DateTimeType),
                th.Property("date", th.DateTimeType),
                th.Property("id", th.StringType),
                th.Property("lastUpdated", th.DateTimeType),
                th.Property("lastUpdatedById", th.StringType),
                th.Property("memo", th.StringType),
                th.Property("objectState", th.StringType),
                th.Property("sendTo", th.StringType),
                th.Property("sendToId", th.StringType),
                th.Property("submitTo", th.StringType),
                th.Property("token", th.NumberType),
                th.Property("type", th.StringType),
                th.Property("version", th.StringType),
        ))),
        th.Property("createdById", th.StringType),
        th.Property("createdOn", th.DateTimeType),
        th.Property("lastUpdated", th.DateTimeType),
        th.Property("lastUpdatedById", th.StringType),
        th.Property("objectState", th.StringType),
        th.Property("paymentStatus", th.StringType),
        th.Property("token", th.NumberType),
        th.Property("version", th.StringType),
    ).to_dict()


class ClientStream(BQEStream):
    name = "client"
    path = "client"
    primary_keys: t.ClassVar[list[str]] = ["id"]
    replication_key = "lastUpdated"
    schema = th.PropertiesList(
        th.Property("name", th.StringType),
        th.Property("clientSince", th.DateTimeType),
        th.Property("company", th.StringType),
        th.Property("createdById", th.UUIDType),
        th.Property("createdOn", th.DateTimeType),
        th.Property("defaultGroup", th.StringType),
        th.Property("defaultGroupId", th.UUIDType),
        th.Property("feeScheduleId", th.UUIDType),
        th.Property("feeScheduleName", th.StringType),
        th.Property("firstName", th.StringType),
        th.Property("id", th.UUIDType),
        th.Property("lastName", th.StringType),
        th.Property("lastUpdated", th.DateTimeType),
        th.Property("mainExpenceTax", th.NumberType),
        th.Property("mainServiceTax", th.NumberType),
    ).to_dict()

class EmployeeStream(BQEStream):
    name = "employee"
    path = "employee"
    replication_key = "lastUpdated"
    schema = th.PropertiesList(
        th.Property("billRate", th.NumberType),
        th.Property("costRate", th.NumberType),
        th.Property("displayName", th.StringType),
        th.Property("firstName", th.StringType),
        th.Property("lastName", th.StringType),
        th.Property("status", th.StringType),
        th.Property("autoApproveExpenseEntry", th.BooleanType),
        th.Property("autoApproveTimeEntry", th.BooleanType),
        th.Property("autoDeposit", th.BooleanType),
        th.Property("autoOverTime", th.BooleanType),
        th.Property("company", th.StringType),
        th.Property("compTimeHours", th.NumberType),
        th.Property("dailyStandardHours", th.NumberType),
        th.Property("dateHired", th.DateTimeType),
        th.Property("dateOfBirth", th.DateTimeType),
        th.Property("dateReleased", th.DateTimeType),
        th.Property("defaultGroup", th.StringType),
        th.Property("defaultGroupId", th.UUIDType),
        th.Property("department", th.StringType),
        th.Property("id", th.UUIDType),
        th.Property("lastUpdated", th.DateTimeType),
        th.Property("manager", th.StringType),
        th.Property("managerId", th.UUIDType),
        th.Property("memo", th.StringType),
        th.Property("middleInitial", th.StringType),
        th.Property("overheadFactor", th.NumberType),
        th.Property("overtimeBillRate", th.NumberType),
        th.Property("overtimeCostRate", th.NumberType),
        th.Property("payableAccount", th.StringType),
        th.Property("payableAccountId", th.UUIDType),
        th.Property("salary", th.NumberType),
        th.Property("salaryPayPeriod", th.StringType),
        th.Property("salutation", th.StringType),
        th.Property("securityProfile", th.StringType),
        th.Property("securityProfileId", th.UUIDType),
        th.Property("title", th.StringType),
        th.Property("token", th.IntegerType),
        th.Property("utilizationTarget", th.NumberType),
        th.Property("weeklyStandardHours", th.NumberType),
    ).to_dict()

class InvoiceStream(BQEStream):
    name = "invoice"
    path = "invoice"
    replication_key = "lastUpdated"
    schema = th.PropertiesList(
        th.Property("date", th.DateTimeType),
        th.Property("invoiceAmount", th.NumberType),
        th.Property("invoiceNumber", th.StringType),
        th.Property("balance", th.NumberType),
        th.Property("billingContactId", th.UUIDType),
        th.Property("dueDate", th.DateTimeType),
        th.Property("expenseAmount", th.NumberType),
        th.Property("expenseTaxAmount", th.NumberType),
        th.Property("fixedFee", th.NumberType),
        th.Property("id", th.UUIDType),
        th.Property("invoiceFrom", th.DateTimeType),
        th.Property("invoiceTo", th.DateTimeType),
        th.Property("lastUpdated", th.DateTimeType),
        th.Property("mainExpenseTax", th.NumberType),
        th.Property("mainServiceTax", th.NumberType),
        th.Property("messageOnInvoice", th.StringType),
        th.Property("miscellaneousAmount", th.NumberType),
        th.Property("purchaseOrderNumber", th.StringType),
        th.Property("referenceNumber", th.StringType),
        th.Property("rfNumber", th.StringType, description="Same a referenceNumber but for electronic invoices"),
        th.Property("serviceAmount", th.NumberType),
        th.Property("serviceTaxAmount", th.NumberType),
        th.Property("token", th.IntegerType),
    ).to_dict()

class PaymentStream(BQEStream):
    name = "payment"
    path = "payment"
    replication_key = "lastUpdated"
    schema = th.PropertiesList(
        th.Property("amount", th.NumberType),
        th.Property("date", th.DateTimeType),
        th.Property("assetAccount", th.StringType),
        th.Property("assetAccountId", th.UUIDType),
        th.Property("badDebtExpenseAccount", th.StringType),
        th.Property("badDebtExpenseAccountId", th.UUIDType),
        th.Property("client", th.StringType),
        th.Property("id", th.UUIDType),
        th.Property("isRetairner", th.BooleanType),
        th.Property("lastUpdated", th.DateTimeType),
        th.Property("liablityAccount", th.StringType),
        th.Property("liablityAccountId", th.UUIDType),
        th.Property("memo", th.StringType),
        th.Property("notes", th.StringType, description="Serves the same function as memo"),
        th.Property("project", th.StringType),
        th.Property("projectId", th.UUIDType),
        th.Property("reference", th.StringType),
        th.Property("token", th.IntegerType),
    ).to_dict()

class ProjectStream(BQEStream):
    name = "project"
    path = "project"
    replication_key = "lastUpdated"
    schema = th.PropertiesList(
        th.Property("code", th.StringType),
        th.Property("managerId", th.UUIDType),
        th.Property("name", th.StringType, description="Project name"),
        th.Property("billingContact", th.StringType),
        th.Property("billingContactId", th.UUIDType),
        th.Property("budgetId", th.UUIDType),
        th.Property("budgetName", th.StringType),
        th.Property("class", th.StringType),
        th.Property("classId", th.UUIDType),
        th.Property("client", th.StringType),
        th.Property("completedOn", th.DateTimeType),
        th.Property("contractAmount", th.NumberType),
        th.Property("currencyMultiplierId", th.UUIDType),
        th.Property("currnecyName", th.StringType),
        th.Property("defaultGroup", th.StringType),
        th.Property("defaultGroupId", th.UUIDType),
        th.Property("displayName", th.StringType),
        th.Property("dueDate", th.DateTimeType),
        th.Property("estimateId", th.UUIDType),
        th.Property("estimateName", th.StringType),
        th.Property("expenseAccount", th.StringType),
        th.Property("expenseAccountId", th.UUIDType),
        th.Property("expenseContract", th.NumberType),
        th.Property("feeScheduleId", th.UUIDType),
        th.Property("feeScheduleName", th.StringType),
        th.Property("fixedfeePercent", th.NumberType),
        th.Property("graceDays", th.IntegerType),
        th.Property("hasChild", th.BooleanType),
        th.Property("hasCustomInvoiceNumber", th.BooleanType),
        th.Property("id", th.UUIDType),
        th.Property("incomeAccount", th.StringType),
        th.Property("incomeAccountId", th.UUIDType),
        th.Property("interestRate", th.NumberType),
        th.Property("invoiceNumber", th.StringType),
        th.Property("invoicePrefix", th.StringType),
        th.Property("invoiceSuffix", th.StringType),
        th.Property("invoiceTemplate", th.StringType),
        th.Property("invoiceTemplateId", th.UUIDType),
        th.Property("jointInvoiceTemplate", th.StringType),
        th.Property("jointInvoiceTemplateId", th.UUIDType),
        th.Property("lastUpdated", th.DateTimeType),
        th.Property("level", th.IntegerType),
        th.Property("liablilityAccount", th.StringType),
        th.Property("liabalilityAccountId", th.UUIDType),
        th.Property("mainExpenseTax", th.NumberType),
        th.Property("mainServiceTax", th.NumberType),
        th.Property("manager", th.StringType),
        th.Property("manualInvoiceTemplate", th.StringType),
        th.Property("manualInvoiceTemplateId", th.UUIDType),
        th.Property("memo", th.StringType),
        th.Property("messageOnInvoice", th.StringType),
        th.Property("originiator", th.StringType),
        th.Property("originiatorId", th.UUIDType),
        th.Property("parent", th.StringType),
        th.Property("parentId", th.UUIDType),
        th.Property("percentComplete", th.NumberType),
        th.Property("percentDescription", th.StringType),
        th.Property("phaseDescription", th.StringType),
        th.Property("phaseName", th.StringType),
        th.Property("phaseOrder", th.IntegerType),
        th.Property("principal", th.StringType),
        th.Property("principalId", th.UUIDType),
        th.Property("purchaseOrderNumber", th.StringType),
        th.Property("receivableAccount", th.StringType),
        th.Property("receivableAccountId", th.UUIDType),
        th.Property("recurringAccount", th.StringType),
        th.Property("recurringAccountId", th.UUIDType),
        th.Property("retainageLimit", th.NumberType),
        th.Property("retainagePercent", th.NumberType),
        th.Property("rootProject", th.StringType),
        th.Property("rootProjectId", th.UUIDType),
        th.Property("serviceContract", th.NumberType),
        th.Property("serviceContract", th.NumberType),
        th.Property("startDate", th.DateTimeType),
        th.Property("term", th.StringType),
        th.Property("termId", th.UUIDType),
        th.Property("token", th.IntegerType),
    ).to_dict()

class TimeEntryStream(BQEStream):
    name = "timeEntry"
    path = "timeEntry"
    replication_key = "lastUpdated"
    schema = th.PropertiesList(
        th.Property("activityId", th.UUIDType),
        th.Property("date", th.DateTimeType),
        th.Property("description", th.StringType),
        th.Property("projectId", th.UUIDType),
        th.Property("resourceId", th.UUIDType),
        th.Property("activity", th.StringType),
        th.Property("actualHours", th.NumberType),
        th.Property("billable", th.BooleanType),
        th.Property("billRate", th.NumberType),
        th.Property("class", th.StringType),
        th.Property("classId", th.UUIDType),
        th.Property("client", th.StringType),
        th.Property("clientHours", th.NumberType),
        th.Property("compensationTime", th.BooleanType),
        th.Property("costRate", th.NumberType),
        th.Property("expenseAccount", th.StringType),
        th.Property("expenseAccountId", th.UUIDType),
        th.Property("extra", th.BooleanType),
        th.Property("flag1", th.BooleanType),
        th.Property("flag2", th.BooleanType),
        th.Property("flag3", th.BooleanType),
        th.Property("id", th.UUIDType),
        th.Property("incomeAccount", th.StringType),
        th.Property("incomeAccountId", th.UUIDType),
        th.Property("invoiceId", th.UUIDType),
        th.Property("invoiceNumber", th.StringType),
        th.Property("lastUpdated", th.DateTimeType),
        th.Property("memo", th.StringType),
        th.Property("overtime", th.BooleanType),
        th.Property("project", th.StringType),
        th.Property("resource", th.StringType),
        th.Property("startInterval", th.IntegerType),
        th.Property("startTime", th.StringType),
        th.Property("stopInterval", th.IntegerType),
        th.Property("stopTime", th.StringType),
        th.Property("tax1", th.NumberType),
        th.Property("tax2", th.NumberType),
        th.Property("tax3", th.NumberType),
        th.Property("token", th.IntegerType),
        th.Property("vendorBillId", th.UUIDType),
        th.Property("vendorBillNumber", th.StringType),
        th.Property("wudMultiplier", th.NumberType),
        th.Property("wudPercent", th.NumberType),
    ).to_dict()